const elixir = require('laravel-elixir');
Elixir.config.assetsPath = 'addons/default/pixney/pyrotheme-theme/resources/assets/src/';
Elixir.config.publicPath = 'addons/default/pixney/pyrotheme-theme/resources/assets/';
require('laravel-elixir-vue');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {
    mix.sass('theme.scss')
    .webpack('app.js');
});
